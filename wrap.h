/*
 * © Copyright 2022 Icecream95
 * © Copyright 2017-2018 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU license.
 *
 * A copy of the license is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#ifndef __WRAP_H__
#define __WRAP_H__

#define PUBLIC __attribute__((visibility("default")))

#define PROLOG(func)                                    \
        static typeof(func) *orig_##func = NULL;        \
        if (!orig_##func)                               \
          orig_##func = dlsym(RTLD_NEXT, #func);        \

typedef uint64_t rock_ptr;

#ifndef _LIST_H_
struct list {
    struct list *next, *prev;
};
#endif

struct rock_bo {
        uint32_t handle;
        uint32_t flags;
        uint64_t size;
        uint64_t obj_va;
        uint64_t dma_va;

        uint64_t mmap_offset;
        void *cpu;
        uint64_t used_size;

        struct list link;
};

void rockwrap_track_allocation(struct rock_bo tmpl);
void rockwrap_track_get_handle(uint32_t handle, uint64_t offset);
void rockwrap_track_mmap(uint64_t offset, void *addr, size_t length,
                         int prot, int flags);

void rockwrap_mem_dump(unsigned num, bool after);

#endif /* __WRAP_H__ */

/*
 * © Copyright 2022 Icecream95
 * © Copyright 2017 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include <dlfcn.h>
#include <fcntl.h>
#include <pthread.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>

#include "wrap.h"

#include "rknpu_ioctl.h"

static pthread_mutex_t l;
static void __attribute__((constructor)) panloader_constructor(void) {
        pthread_mutexattr_t mattr;

        pthread_mutexattr_init(&mattr);
        pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&l, &mattr);
        pthread_mutexattr_destroy(&mattr);
}

#define LOCK()   pthread_mutex_lock(&l);
#define UNLOCK() pthread_mutex_unlock(&l)

#define IOCTL_CASE(request) (_IO(_IOC_TYPE(request), _IOC_NR(request)))

int rock_fd = 0;

typedef int (open_func)(const char *, int flags, ...);

/**
 * Overriden libc functions start here
 */
static inline int
rockwrap_open_wrap(open_func *func, const char *path, int flags, va_list args)
{
        mode_t mode = 0;
        int ret;

        if (flags & O_CREAT) {
                mode = (mode_t) va_arg(args, int);
                ret = func(path, flags, mode);
        } else {
                ret = func(path, flags);
        }

        printf("PATH!!! %s\n", path);

        LOCK();
        if (ret != -1 &&
            (strcmp(path, "/dev/dri/card1") == 0 ||
             strcmp(path, "/dev/dri/renderD129") == 0)) {
                rock_fd = ret;
                printf("rock_fd = %i\n", ret);
        }
        UNLOCK();

        return ret;
}

PUBLIC int
open(const char *path, int flags, ...)
{
        PROLOG(open);
        va_list args;
        va_start(args, flags);
        int o = rockwrap_open_wrap(orig_open, path, flags, args);
        va_end(args);
        return o;
}

PUBLIC int
open64(const char *path, int flags, ...)
{
        PROLOG(open64);
        va_list args;
        va_start(args, flags);
        int o = rockwrap_open_wrap(orig_open64, path, flags, args);
        va_end(args);
        return o;
}

PUBLIC int
close(int fd)
{
        PROLOG(close);

        /* Intentionally racy: prevents us from trying to hold the global mutex
         * in calls from system libraries */
        if (fd <= 0 || !rock_fd || fd != rock_fd)
                return orig_close(fd);

        LOCK();
        if (!fd || fd != rock_fd) {
                rock_fd = 0;
        }
        UNLOCK();

        return orig_close(fd);
}

PUBLIC int
ioctl(int fd, unsigned long request, ...)
{
        PROLOG(ioctl);

        va_list args;
        va_start(args, request);
        void *ptr = va_arg(args, void *);
        va_end(args);

        if (!fd || fd != rock_fd)
                return orig_ioctl(fd, request, ptr);

        LOCK();

        static int submit_count = 0;

        if (IOCTL_CASE(request) == IOCTL_CASE(DRM_IOCTL_RKNPU_SUBMIT)) {
                ++submit_count;
                rockwrap_mem_dump(submit_count, false);
        }

        printf("ioctl %lx: ", request);
        int ret = orig_ioctl(fd, request, ptr);

        switch (IOCTL_CASE(request)) {
        case IOCTL_CASE(DRM_IOCTL_RKNPU_ACTION):
                printf("Get/Set");
                break;
        case IOCTL_CASE(DRM_IOCTL_RKNPU_SUBMIT): {
                struct rknpu_submit *args = ptr;

                // TODO: Dump subcore tasks
                printf("Submit flags 0x%x timeout %u start %u num %u count %u prio %i obj %llx reg %llx task %llx user %llx cores %x fence %i",
                       args->flags, args->timeout,
                       args->task_start, args->task_number, args->task_counter,
                       args->priority,
                       args->task_obj_addr, args->regcfg_obj_addr, args->task_base_addr,
                       args->user_data, args->core_mask, args->fence_fd);

                // TODO: Properly wait for the job to finish
                usleep(1000000);

                rockwrap_mem_dump(submit_count, true);
                break;
        }
        case IOCTL_CASE(DRM_IOCTL_RKNPU_MEM_CREATE): {
                struct rknpu_mem_create *args = ptr;

                printf("Create handle %i flags 0x%x size 0x%llx obj 0x%llx dma 0x%llx",
                       args->handle, args->flags, args->size, args->obj_addr, args->dma_addr);

                struct rock_bo tmpl = {
                        .handle = args->handle,
                        .flags = args->flags,
                        .size = args->size,
                        .obj_va = args->obj_addr,
                        .dma_va = args->dma_addr,
                };
                rockwrap_track_allocation(tmpl);

                break;
	}
        case IOCTL_CASE(DRM_IOCTL_RKNPU_MEM_MAP): {
                struct rknpu_mem_map *args = ptr;

                printf("Map handle %i offest 0x%llx",
                       args->handle, args->offset);

                rockwrap_track_get_handle(args->handle, args->offset);

                break;
        }
        case IOCTL_CASE(DRM_IOCTL_RKNPU_MEM_DESTROY):
                printf("Mem destroy");
                break;
        case IOCTL_CASE(DRM_IOCTL_RKNPU_MEM_SYNC):
                printf("Mem sync");
                break;
	}

        printf("\n");

        UNLOCK();

        return ret;
}

typedef void* (mmap_func)(void *, size_t, int, int, int, loff_t);

static inline void *
rockwrap_mmap_wrap(mmap_func *func,
                   void *addr, size_t length, int prot,
                   int flags, int fd, loff_t offset)
{
        void *ret = func(addr, length, prot, flags, fd, offset);

        if (!fd || fd != rock_fd)
                return ret;

        LOCK();

        rockwrap_track_mmap(offset, ret, length, prot, flags);

        UNLOCK();
        return ret;
}

PUBLIC void *
mmap64(void *addr, size_t length, int prot, int flags, int fd,
       loff_t offset)
{
        PROLOG(mmap64);

        return rockwrap_mmap_wrap(orig_mmap64, addr, length, prot, flags, fd,
                                  offset);
}

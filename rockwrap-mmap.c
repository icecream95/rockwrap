/*
 * © Copyright 2022 Icecream95
 * © Copyright 2017 The Panfrost Community
 *
 * This program is free software and is provided to you under the terms of the
 * GNU General Public License version 2 as published by the Free Software
 * Foundation, and any use by you of this program is subject to the terms
 * of such GNU licence.
 *
 * A copy of the licence is included with the program, and can also be obtained
 * from Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

#include <fcntl.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/types.h>

#include "list.h"

#include "wrap.h"

static LIST_HEAD(bos);

void
rockwrap_track_allocation(struct rock_bo tmpl)
{
        struct rock_bo *bo = malloc(sizeof(*bo));
        *bo = tmpl;

        list_init(&bo->link);
        list_add(&bo->link, &bos);
}

void
rockwrap_track_get_handle(uint32_t handle, uint64_t offset)
{
        struct rock_bo *pos, *bo = NULL;

        list_for_each_entry(pos, &bos, link) {
                if (pos->handle == handle) {
                        bo = pos;
                        break;
                }
        }

        if (!bo) {
                fprintf(stderr, "Could not find handle %u for track_get_handle\n", handle);
                return;
        }

        bo->mmap_offset = offset;
}

void
rockwrap_track_mmap(uint64_t offset, void *addr, size_t length,
                    int prot, int flags)
{
        struct rock_bo *pos, *bo = NULL;

        list_for_each_entry(pos, &bos, link) {
                if (pos->mmap_offset == offset) {
                        bo = pos;
                        break;
                }
        }

        if (!bo) {
                fprintf(stderr, "Could not find offset 0x%lx for mmap\n", offset);
                return;
        }

        bo->cpu = addr;
        bo->used_size = length;
}

/* From src/panfrost/lib/genxml/decode.h in Mesa */
static inline void
pan_hexdump(FILE *fp, const uint8_t *hex, size_t cnt, bool with_strings)
{
        for (unsigned i = 0; i < cnt; ++i) {
                if ((i & 0xF) == 0)
                        fprintf(fp, "%06X  ", i);

                uint8_t v = hex[i];

                if (v == 0 && (i & 0xF) == 0) {
                        /* Check if we're starting an aligned run of zeroes */
                        unsigned zero_count = 0;

                        for (unsigned j = i; j < cnt; ++j) {
                                if (hex[j] == 0)
                                        zero_count++;
                                else
                                        break;
                        }

                        if (zero_count >= 32) {
                                fprintf(fp, "*\n");
                                i += (zero_count & ~0xF) - 1;
                                continue;
                        }
                }

                fprintf(fp, "%02X ", hex[i]);
                if ((i & 0xF) == 0xF && with_strings) {
                        fprintf(fp, " | ");
                        for (unsigned j = i & ~0xF; j <= i; ++j) {
                                uint8_t c = hex[j];
                                fputc((c < 32 || c > 128) ? '.' : c, fp);
                        }
                }

                if ((i & 0xF) == 0xF)
                        fprintf(fp, "\n");
        }

        fprintf(fp, "\n");
}

void
rockwrap_mem_dump(unsigned num, bool after)
{
        char *fname;
        asprintf(&fname, "rockwrap.dump.%04u.%u", num, after);
        FILE *f = fopen(fname, "w");
        free(fname);

        asm volatile("dmb sy" ::: "memory");

        struct rock_bo *bo;
        list_for_each_entry(bo, &bos, link) {
                fprintf(f, "BO %i 0x%lx-0x%lx 0x%lx-0x%lx (size 0x%lx) flags 0x%x\n",
                        bo->handle,
                        bo->obj_va, bo->obj_va + bo->size,
                        bo->dma_va, bo->dma_va + bo->size,
                        bo->size, bo->flags);

                if (!bo->cpu)
                        continue;

                for (uint64_t i = 0; i < bo->size; i += 64)
                        asm volatile("dc civac, %0" ::
                                     "r" (bo->cpu + i) :
                                     "memory");

                pan_hexdump(f, bo->cpu, bo->size, false);
        }

        fclose(f);
}
